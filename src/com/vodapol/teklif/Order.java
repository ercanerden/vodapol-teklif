package com.vodapol.teklif;

import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.EditText;
import java.math.BigDecimal;
import java.util.ArrayList;

public final class Order {
    private static ArrayList<OrderItem> order = new ArrayList<OrderItem>();

    public static void addItem(){
        order.add(new OrderItem());
        Log.d("Order size->>", Integer.toString(order.size()));

    }

    //Takes the RelativeLayout that contains the item information.
    //Returns the updated OrderItem to be used in the view.
    public static OrderItem updateItem(RelativeLayout v){
        int index = getIndex(v);
        BigDecimal price = new BigDecimal("0"+((TextView)v.findViewById(R.id.product_price)).getText().toString());
        BigDecimal quantity = new BigDecimal("0"+((TextView)v.findViewById(R.id.product_quantity)).getText().toString());
        String name = ((TextView)v.findViewById(R.id.product_name)).getText().toString();
        String diameter = ((TextView)v.findViewById(R.id.diameter)).getText().toString();
        String length = ((TextView)v.findViewById(R.id.length)).getText().toString();
        String height = ((TextView)v.findViewById(R.id.height)).getText().toString();
        order.get(index).update(name,quantity,price,diameter,length,height);
        return order.get(index);
    }
    //Finds the index of the RelativeLayout with the product information
    //in its parent contatiner. The view index and index of the OrderItem in
    //the Order ArrayList are(and should stay) the same.
    private static Integer getIndex(RelativeLayout v){
        return ((LinearLayout)v.getParent()).indexOfChild(v);
    }
    public static boolean deleteItem(RelativeLayout v){
        int index = getIndex(v);

        if(order.remove(index) != null){
            Log.d("Order size->>", Integer.toString(order.size()));
            return true;
        }else{
            Log.d("Order size->>", Integer.toString(order.size()));
            return false;
        }
    }
    //TODO: This can be moved to somewhere where it will be called less 
    //frequently.
    public static BigDecimal getSubTotal(){
        BigDecimal total = new BigDecimal(0);
        for (OrderItem i : order){
           total =  total.add(i.getTotal()).setScale(2,BigDecimal.ROUND_UP);
        }
        return total;
    }
    public static BigDecimal getTax(){
        return getSubTotal().multiply(new BigDecimal(0.18)).setScale(2,BigDecimal.ROUND_UP);
    }
    public static BigDecimal getGrandTotal(){
        return getSubTotal().add(getTax()).setScale(2,BigDecimal.ROUND_UP);
    }
    public static ArrayList getOrderItems(){
        return order;
    }

}
