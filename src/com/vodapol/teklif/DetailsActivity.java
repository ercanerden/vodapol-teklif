package com.vodapol.teklif;

import java.math.BigDecimal;
import android.net.Uri;
import android.content.Intent;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import java.io.ByteArrayOutputStream;
import android.widget.EditText;
import android.widget.Toast;
import java.util.ArrayList;
import java.io.InputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.File;
import android.view.View;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;
import android.app.Activity;
import android.os.Bundle;

import harmony.java.awt.Color;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Element;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;



public class DetailsActivity extends Activity {
    @Override
    public void onCreate(Bundle SavedInstanceState){
        super.onCreate(SavedInstanceState);
        setContentView(R.layout.details);
        updateHeader();
    }

    //There's a clone of it in the home activity. Moving it to the MyApp class
    //might make sense.
    public void updateHeader(){
        ((TextView)findViewById(R.id.subtotal))
            .setText(Order.getSubTotal().toString());

        ((TextView)findViewById(R.id.tax))
            .setText(Order.getTax().toString());

        ((TextView)findViewById(R.id.grand_total))
            .setText(Order.getGrandTotal().toString());

    }
    //Big block
    //Returns the full path to the pdf file created.
    public String createDocument(){
        //rotate() is used to create the page in landscape orientation.
        Document document = new Document(PageSize.A4.rotate());
        String file = Environment.getExternalStorageDirectory().getPath() + "/Teklif.pdf";
        try{
            PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream(file));
            document.open();

            //This is here because the guy who made the library says so.
            //Turkish characters were missing from the pdf and this solves it.
            InputStream inputStream = this.getResources().openRawResource(R.raw.arialbd);
            byte[] buffer= new byte[inputStream.available()];
            inputStream.read(buffer);
            BaseFont bfb = BaseFont.createFont("arialbd.ttf", 
                    BaseFont.IDENTITY_H, true, false, buffer, null);
            Font titleFont = new Font(bfb, 10);
            Font logoFont = new Font(bfb, 64);
            logoFont.setColor(Color.RED);

            inputStream = this.getResources().openRawResource(R.raw.arial);
            buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            BaseFont bf = BaseFont.createFont("arial.ttf", 
                    BaseFont.IDENTITY_H, true, false, buffer, null);
            Font defaultFont = new Font(bf, 9);




            //Top of the page
            PdfPTable topTable = new PdfPTable(3);
            topTable.setWidthPercentage(100);
            topTable.setSpacingAfter(6);
            topTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
            topTable.getDefaultCell().setPadding(0);;


            //Logo-column table
            PdfPTable logoColumnTable = new PdfPTable(1);
            logoColumnTable.setWidthPercentage(100);
            logoColumnTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
            logoColumnTable.getDefaultCell().setPaddingTop(-10);
            logoColumnTable.getDefaultCell().setPaddingBottom(16);
            logoColumnTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);

            Paragraph logoText = new Paragraph("vodapol", logoFont);
            PdfPCell contactInfo = new PdfPCell(new Paragraph("Vodapol Banyo İnş. San. Tic. Ltd. Şti.\nSapanbağları Mh. Süreyyapaşa Cd. No: 87 Pendik - İstanbul\nVD. Kartal - V. No: 925 039 8924\nİş Bankası iban:TR 960006400000112190265944\nTel: (216) 375 3435", defaultFont));
            contactInfo.setHorizontalAlignment(Element.ALIGN_CENTER);
            contactInfo.setPadding(10);
            logoColumnTable.addCell(logoText);
            logoColumnTable.addCell(contactInfo);

            //Date-stamp table
            PdfPTable dateStampTable = new PdfPTable(1);
            dateStampTable.setWidthPercentage(100);
            dateStampTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
            dateStampTable.getDefaultCell().setPaddingTop(10);
            dateStampTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();;
            Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(),R.drawable.stamp);
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);
            Image stampImage = Image.getInstance(stream.toByteArray());


            Date today = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy", getResources().getConfiguration().locale);

            dateStampTable.addCell(new Paragraph("TARİH:"+formatter.format(today), titleFont));;
            dateStampTable.addCell(Image.getInstance(stream.toByteArray()));

            //Customer approve table
            PdfPTable approveTable = new PdfPTable(1);
            approveTable.getDefaultCell().disableBorderSide(Rectangle.LEFT);
            approveTable.getDefaultCell().disableBorderSide(Rectangle.RIGHT);
            approveTable.getDefaultCell().disableBorderSide(Rectangle.TOP);
            approveTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);

            approveTable.addCell(new Paragraph("\n\n\nMÜŞTERİ ONAY", titleFont));
            approveTable.addCell(new Paragraph("\n\n\n\n\n\n\n\n\n", defaultFont));

            PdfPCell approveTableCell = new PdfPCell();
            approveTableCell.addElement(approveTable);
            approveTableCell.setBorder(Rectangle.NO_BORDER);


            topTable.addCell(logoColumnTable);
            topTable.addCell(dateStampTable);
            topTable.addCell(approveTableCell);

            document.add(topTable);

            //--End top of the page

            //Proucts table
            PdfPTable productsTable = new PdfPTable(7);
            productsTable.setWidthPercentage(100);
            float[] productsColumnWidths = {5,1,2,2,1,2,2};
            productsTable.setWidths(productsColumnWidths);
            productsTable.setSpacingAfter(6);

            //Create the hader.
            ArrayList<PdfPCell> headerRow = new ArrayList<PdfPCell>();
            headerRow.add(new PdfPCell(new Paragraph("ÜRÜN/HİZMET AÇIKLAMASI", titleFont)));
            headerRow.add(new PdfPCell(new Paragraph("ÇAP(cm)", titleFont))); 
            headerRow.add(new PdfPCell(new Paragraph("UZUNLUK(cm)", titleFont))); 
            headerRow.add(new PdfPCell(new Paragraph("YÜKSEKLİK(cm)", titleFont))); 
            headerRow.add(new PdfPCell(new Paragraph("ADET", titleFont))); 
            headerRow.add(new PdfPCell(new Paragraph("FİYAT(TL)", titleFont))); 
            headerRow.add(new PdfPCell(new Paragraph("TUTAR(TL)", titleFont))); 

            for(PdfPCell cell : headerRow){

                cell.setBackgroundColor(Color.LIGHT_GRAY);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setVerticalAlignment(Element.ALIGN_CENTER);
                productsTable.addCell(cell);

            }

            //Insert products into the table.
            ArrayList<OrderItem> items = Order.getOrderItems();
            for(OrderItem i : items){
                PdfPCell name = new PdfPCell(new Paragraph(i.getName(), defaultFont)); 
                name.setHorizontalAlignment(Element.ALIGN_CENTER);
                productsTable.addCell(name);

                PdfPCell diameter = new PdfPCell(new Paragraph(i.getDiameter(), defaultFont)); 
                diameter.setHorizontalAlignment(Element.ALIGN_CENTER);
                productsTable.addCell(diameter);

                PdfPCell length = new PdfPCell(new Paragraph(i.getLength(), defaultFont)); 
                length.setHorizontalAlignment(Element.ALIGN_CENTER);
                productsTable.addCell(length);

                PdfPCell height = new PdfPCell(new Paragraph(i.getHeight(), defaultFont)); 
                height.setHorizontalAlignment(Element.ALIGN_CENTER);
                productsTable.addCell(height);

                PdfPCell quantity = new PdfPCell(new Paragraph(i.getQuantity().toString(), defaultFont)); 
                quantity.setHorizontalAlignment(Element.ALIGN_CENTER);
                productsTable.addCell(quantity);

                PdfPCell price = new PdfPCell(new Paragraph(i.getPrice().toString(), defaultFont)); 
                price.setHorizontalAlignment(Element.ALIGN_CENTER);
                productsTable.addCell(price);

                PdfPCell total = new PdfPCell(new Paragraph(i.getTotal().toString(), defaultFont)); 
                total.setHorizontalAlignment(Element.ALIGN_CENTER);
                productsTable.addCell(total);
            }


            //Insert some (max 12) empty lines so the page looks somewhat busy.
            int productDiff = 12 - items.size();
            for( int i = 0; i < productDiff; i++){
                //The table has seven columns.
                for(int j = 0; j < 7; j++){
                    productsTable.addCell(new PdfPCell(new Paragraph(" ", defaultFont)));
                }

            }

            //Totals table
            PdfPTable totalsTable = new PdfPTable(2);
            totalsTable.setWidthPercentage(100);

            PdfPCell subTotalTitleCell = new PdfPCell(new Paragraph("ARA TOPLAM", titleFont));
            subTotalTitleCell.setBackgroundColor(Color.GRAY);
            subTotalTitleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            subTotalTitleCell.setBackgroundColor(Color.LIGHT_GRAY);

            PdfPCell subTotalCell = new PdfPCell(new Paragraph(Order.getSubTotal().toString(), defaultFont));
            subTotalCell.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell taxTitleCell = new PdfPCell(new Paragraph("KDV", titleFont));
            taxTitleCell.setBackgroundColor(Color.GRAY);
            taxTitleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            taxTitleCell.setBackgroundColor(Color.LIGHT_GRAY);

            PdfPCell taxCell = new PdfPCell(new Paragraph(Order.getTax().toString(), defaultFont));
            taxCell.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell grandTotalTitleCell = new PdfPCell(new Paragraph("GENEL TOPLAM", titleFont));
            grandTotalTitleCell.setBackgroundColor(Color.GRAY);
            grandTotalTitleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            grandTotalTitleCell.setBackgroundColor(Color.LIGHT_GRAY);

            PdfPCell grandTotalCell = new PdfPCell(new Paragraph(Order.getGrandTotal().toString(), defaultFont));
            grandTotalCell.setHorizontalAlignment(Element.ALIGN_CENTER);

            totalsTable.addCell(subTotalTitleCell);
            totalsTable.addCell(subTotalCell);

            totalsTable.addCell(taxTitleCell);
            totalsTable.addCell(taxCell);

            totalsTable.addCell(grandTotalTitleCell);
            totalsTable.addCell(grandTotalCell);


            PdfPCell totalsPaddingCell = new PdfPCell();
            totalsPaddingCell.setColspan(5);
            totalsPaddingCell.setBorderWidthRight(0);
            totalsPaddingCell.setBorderWidthBottom(0);
            totalsPaddingCell.setBorderWidthLeft(0);

            PdfPCell totalsTableCell = new PdfPCell();
            totalsTableCell.setColspan(2);
            totalsTableCell.setPadding(0);
            totalsTableCell.addElement(totalsTable);



            //Details table
            PdfPTable detailsTable = new PdfPTable(2);
            detailsTable.setWidthPercentage(100);
            float[] detailsColumnWidths = {3,12};
            detailsTable.setWidths(detailsColumnWidths);

            PdfPCell emptyCell = new PdfPCell(new Paragraph(" ", titleFont));
            emptyCell.setBorderWidthLeft(0);
            emptyCell.setBorderWidthRight(0);
            emptyCell.setBorderWidthTop(0);
            emptyCell.setBorderWidthBottom(0);
            emptyCell.setFixedHeight(6);
            detailsTable.addCell(emptyCell); detailsTable.addCell(emptyCell);

            PdfPCell infoTitleCell = new PdfPCell(new Paragraph("Teknik Bilgi: ", titleFont));
            infoTitleCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            infoTitleCell.setBackgroundColor(Color.LIGHT_GRAY);
            String infoText = ((EditText)findViewById(R.id.info)).getText().toString();
            PdfPCell infoCell = new PdfPCell(new Paragraph(infoText, defaultFont));
            detailsTable.addCell(infoTitleCell);
            detailsTable.addCell(infoCell);

            detailsTable.addCell(emptyCell); detailsTable.addCell(emptyCell);

            PdfPCell priceTitleCell = new PdfPCell(new Paragraph("Yalnız:", titleFont));
            priceTitleCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            priceTitleCell.setBackgroundColor(Color.LIGHT_GRAY);
            String priceText = ((EditText)findViewById(R.id.price)).getText().toString();
            PdfPCell priceCell = new PdfPCell(new Paragraph(priceText, defaultFont));
            detailsTable.addCell(priceTitleCell);
            detailsTable.addCell(priceCell);

            detailsTable.addCell(emptyCell); detailsTable.addCell(emptyCell);

            PdfPCell expirationTitleCell = new PdfPCell(new Paragraph("Geçerlilik Süresi:", titleFont));
            expirationTitleCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            expirationTitleCell.setBackgroundColor(Color.LIGHT_GRAY);
            String expirationText = ((EditText)findViewById(R.id.expiration)).getText().toString();
            PdfPCell expirationCell = new PdfPCell(new Paragraph(expirationText, defaultFont));
            detailsTable.addCell(expirationTitleCell);
            detailsTable.addCell(expirationCell);

            PdfPCell deliveryTimeTitleCell = new PdfPCell(new Paragraph("Teslim Süresi:", titleFont));
            deliveryTimeTitleCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            deliveryTimeTitleCell.setBackgroundColor(Color.LIGHT_GRAY);
            String deliveryTimeText = ((EditText)findViewById(R.id.deliveryTime)).getText().toString();
            PdfPCell deliveryTimeCell = new PdfPCell(new Paragraph(deliveryTimeText, defaultFont));
            detailsTable.addCell(deliveryTimeTitleCell);
            detailsTable.addCell(deliveryTimeCell);

            PdfPCell deliveryPriceTitleCell = new PdfPCell(new Paragraph("Teslim Ücreti:", titleFont));
            deliveryPriceTitleCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            deliveryPriceTitleCell.setBackgroundColor(Color.LIGHT_GRAY);
            String deliveryPriceText = ((EditText)findViewById(R.id.deliveryPrice)).getText().toString();
            PdfPCell deliveryPriceCell = new PdfPCell(new Paragraph(deliveryPriceText, defaultFont));
            detailsTable.addCell(deliveryPriceTitleCell);
            detailsTable.addCell(deliveryPriceCell);


            PdfPCell paymentTitleCell = new PdfPCell(new Paragraph("Ödeme Şartları:", titleFont));
            paymentTitleCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            paymentTitleCell.setBackgroundColor(Color.LIGHT_GRAY);
            String paymentText = ((EditText)findViewById(R.id.payment)).getText().toString();
            PdfPCell paymentCell = new PdfPCell(new Paragraph(paymentText, defaultFont));
            detailsTable.addCell(paymentTitleCell);
            detailsTable.addCell(paymentCell);

            PdfPCell warrantyTitleCell = new PdfPCell(new Paragraph("Garanti:", titleFont));
            warrantyTitleCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            warrantyTitleCell.setBackgroundColor(Color.LIGHT_GRAY);
            String warrantyText = ((EditText)findViewById(R.id.warranty)).getText().toString();
            PdfPCell warrantyCell = new PdfPCell(new Paragraph(warrantyText, defaultFont));
            detailsTable.addCell(warrantyTitleCell);
            detailsTable.addCell(warrantyCell);

            productsTable.addCell(totalsPaddingCell);
            productsTable.addCell(totalsTableCell);


            document.add(productsTable);
            document.add(detailsTable);

            document.close();
            Log.d("Teklif", file);
            return file;

        }catch (FileNotFoundException e){

            Log.e("Teklif", "exception", e);

        }catch(DocumentException e){

            Log.e("Teklif", "exception", e);
        }
        catch(IOException e){
            Log.e("Teklif", "exception", e);
        }

        return null;
    }
    public void send(View v){
        if(Order.getOrderItems().size() > 0){
            String filePath = createDocument();
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/Message");
            intent.putExtra(Intent.EXTRA_SUBJECT, "Vodapol - Teklif");
            intent.putExtra(Intent.EXTRA_TEXT, "Teklif ektedir.");
            intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///" + filePath));
            startActivity(intent);
        }
        else if (Order.getGrandTotal().compareTo(new BigDecimal(0)) < 1){
            
            Toast.makeText(getApplicationContext(), "Ürünlerin toplamı 0.\nFiyatları kontrol et!", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(getApplicationContext(), "Teklifte hiç ürün yok.", Toast.LENGTH_SHORT).show();
            Log.d("Teklif", "No items in the order. Won't create PDF.");

        }
    }
}
