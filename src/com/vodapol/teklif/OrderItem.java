package com.vodapol.teklif;
import java.math.BigDecimal;

class OrderItem {
    private BigDecimal quantity;
    private BigDecimal price;
    private BigDecimal total;
    private String name;
    private String diameter;
    private String length;
    private String height;

    OrderItem(){
        quantity = new BigDecimal(0);
        price = new BigDecimal(0);
        total = new BigDecimal(0);
        name = "";
        diameter ="";
        length ="";
        height ="";
    }
    public void update(String name, BigDecimal quantity, BigDecimal price, String diameter, String length, String height){
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.total = quantity.multiply(price); 
        this.diameter = diameter;
        this.length = length;
        this.height = height;
    }
    public String getName(){
        return name;
    }

    public BigDecimal getPrice(){
        return price;
    }
    public BigDecimal getQuantity(){
        return quantity;
    }
    public BigDecimal getTotal(){
        return total;
    }
    public String getDiameter(){
        return diameter;
    }
    public String getLength(){
        return length;
    }
    public String getHeight(){
        return height;
    }
    @Override
    public String toString(){

        return  "========== OrderItem =============\n"
               + super.toString() + "\n"
               +"Quantity  : " + quantity  + "\n"
               +"Price     : " + price     + "\n"
               +"Name      : " + name      + "\n"
               +"Diameter  : " + diameter  + "\n"
               +"Length    : " + length    + "\n"
               +"Height    : " + height    + "\n"
               +"========== OrderItem =============";
    }
}
