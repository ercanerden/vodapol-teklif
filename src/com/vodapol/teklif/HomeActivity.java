package com.vodapol.teklif;

import android.content.Intent;
import java.math.BigDecimal;
import android.view.KeyEvent;
import android.widget.TextView.OnEditorActionListener;
import android.util.Log;
import android.widget.EditText;
import android.view.ViewTreeObserver;
import android.content.DialogInterface;
import android.app.AlertDialog;
import android.widget.RelativeLayout;
import android.content.Context;
import android.app.Activity;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

public class HomeActivity extends Activity
{
    private ViewTreeObserver vtObserver;
    private LinearLayout root;
    private MyApp app;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        root = (LinearLayout)findViewById(R.id.root);
        app = (MyApp)getApplication();

    }


    public void details(View view){
        Intent intent = new Intent(this, DetailsActivity.class);
        startActivity(intent);

    }

    //-------------CRUD stuff ---------------------
    //This is where the view is handled. The actual order data is updated by
    // related functions in static Order class functions.
    // Most updates should be done on focus change except the last input
    // field, which has an OnEditorActionListener attached.
    public void addProduct(View view){
        //Data
        Order.addItem();

        //View
        RelativeLayout product = (RelativeLayout) getLayoutInflater()
            .inflate(R.layout.product,root,false);
        root.addView(product);

        final EditText product_name = (EditText)product.findViewById(R.id.product_name);
        product_name.requestFocus();

        EditText product_quantity = 
            (EditText)product.findViewById(R.id.product_quantity);

        EditText product_price = 
            (EditText)product.findViewById(R.id.product_price);
        //This is for updating
        product_price.setOnEditorActionListener(new OnEditorActionListener(){
            @Override
            public boolean onEditorAction(TextView v, int actionId, 
                KeyEvent event){
                updateProduct(v);
                return false;
            }

        });

    }

    public void updateProduct(TextView v){
        //Get the updated object from Order.
        RelativeLayout productView = (RelativeLayout) v.getParent();
        OrderItem orderItem = Order.updateItem(productView);

        //Update the view.
        //TODO: This is messy and verbose. Find a way to make it look.
        ((EditText)productView.findViewById(R.id.product_name))
            .setText(orderItem.getName());

        ((EditText)productView.findViewById(R.id.product_quantity))
            .setText(orderItem.getQuantity().toString());

        ((EditText)productView.findViewById(R.id.product_price))
            .setText(orderItem.getPrice().toString());

        ((TextView)productView.findViewById(R.id.total))
            .setText(orderItem.getTotal().toString());

        ((EditText)productView.findViewById(R.id.diameter))
            .setText(orderItem.getDiameter());

        ((EditText)productView.findViewById(R.id.length))
            .setText(orderItem.getLength());

        ((EditText)productView.findViewById(R.id.height))
            .setText(orderItem.getHeight());

        updateHeader();

        Log.d("Teklif", orderItem.toString());
    }

    public void updateHeader(){
        ((TextView)findViewById(R.id.subtotal))
            .setText(Order.getSubTotal().toString());

        ((TextView)findViewById(R.id.tax))
            .setText(Order.getTax().toString());

        ((TextView)findViewById(R.id.grand_total))
            .setText(Order.getGrandTotal().toString());
    
    }

    public void deleteProduct(View v){
        RelativeLayout productView = (RelativeLayout) v.getParent();
        if(Order.deleteItem(productView)){
            root.removeView(productView);
            updateHeader();
        }
    }

    public void clearOrder(View view){
        new AlertDialog.Builder(this)
            .setMessage("Yaptigin hersey silinecek.\n\nEmin misin?")
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle("Uyari")
            .setPositiveButton("Evet",new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which){
                    root.removeAllViews();
                }

            })
        .setNegativeButton("Hayir",null)
            .show();
    }
}
